package org.verleihsystem.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;
import org.verleihsystem.dao.AnfrageDAO;
import org.verleihsystem.dao.ArtikelDAO;
import org.verleihsystem.dao.ArtikelkategorieDAO;
import org.verleihsystem.dao.BenutzerDAO;
import org.verleihsystem.dao.OrtDAO;
import org.verleihsystem.model.Artikel;
import org.verleihsystem.model.Artikelkategorie;
import org.verleihsystem.model.Benutzer;
import org.verleihsystem.model.Ort;

import org.verleihsystem.test.TestDataFactory;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AusleihanfrageStellenTest {
    

	@LocalServerPort
    private int port;

	//private Long artikelID=1L;//TODO echte id bekommen..?

	@Autowired
    private AnfrageDAO anfrageDAO;
	
	@Autowired
	private BenutzerDAO benutzerDAO;
	
	@Autowired
	private ArtikelkategorieDAO artikelkategorieDAO;
	
	@Autowired
	private ArtikelDAO artikelDAO;
	
	@Autowired
	private OrtDAO ortDAO;
	
	
	//private Benutzer verleiher;
	private Benutzer ausleiher;
	private Benutzer verleiher;
	private Artikelkategorie artikelkategorie;
	private Artikel artikel;

	private HtmlUnitDriver driver = new HtmlUnitDriver();
	
	/**
	 * Setup der Testumgebung
	 * Verleiher, Ausleiher erstellen und persistieren
	 * Ausleiher einloggen
	 * artikel erstellen und persistieren inkl Orte
	 * @author embeach
	 */
	@Before
	public void before() {
		deleteContext();
		verleiher= TestDataFactory.createVerleiher();
		benutzerDAO.save(verleiher);
		ausleiher= TestDataFactory.createAusleiher();
		benutzerDAO.save(ausleiher);
		loginAusleiher();
		artikelkategorie=TestDataFactory.createArtikelkategorie();
		artikelkategorieDAO.save(artikelkategorie);
		artikel=TestDataFactory.createArtikel(verleiher,artikelkategorie);
		artikel=artikelDAO.save(artikel);
		Ort ort1=TestDataFactory.createOrt1(artikel);
		ortDAO.save(ort1);
		Ort ort2=TestDataFactory.createOrt2(artikel);
		ortDAO.save(ort2);
		System.err.println("arktikel created:"+artikel);
	}

	@After
	public void after() {
		this.deleteContext();
	}
	
	private void deleteContext() {
		anfrageDAO.deleteAll();
		artikelDAO.deleteAll();
		benutzerDAO.deleteAll();
		artikelkategorieDAO.deleteAll();
		
	}
	

	
    @Test
    public void createRequestForRentSuccesfully() throws ParseException {
    	Long aktuelleAnfragen = anfrageDAO.count();
    	DateTimeFormatter dtfDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	DateTimeFormatter dtfTime = DateTimeFormatter.ofPattern("HH:mm");
    	LocalDateTime now = LocalDateTime.now();
    	String dateNow = dtfDate.format(now);
    	String time = dtfTime.format(now);
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(sdf.parse(dateNow));
    	cal.add(Calendar.DATE,1);
    	String dateTomorrow = sdf.format(cal.getTime());
    	cal.setTime(sdf.parse(dateTomorrow));
    	cal.add(Calendar.DATE,1);
    	String dateAfterTomorrow = sdf.format(cal.getTime());
        HtmlUnitDriver driver = new HtmlUnitDriver();
        driver.get("http://localhost:"+port+"/login");
        driver.findElementById("Benutzername").sendKeys(ausleiher.getBenutzername());
        driver.findElementById("Passwort").sendKeys(ausleiher.getPasswort());
        driver.findElementById("btn1").click();
        assertEquals("Verleihportal 1.0 | Willkommen!", driver.getTitle());
        driver.get("http://localhost:"+port+"/artikel/"+artikel.getId());
        driver.findElementById("requestBtn").click();
        System.out.println("currentUrl:"+driver.getCurrentUrl());
        assertTrue(driver.getCurrentUrl().contains("ausleihanfrage/"+artikel.getId()));
        driver.findElementById("ausgabeTermin.datumString").sendKeys(dateTomorrow);
        driver.findElementById("rueckgabeTermin.datumString").sendKeys(dateAfterTomorrow);
        driver.findElementById("ausgabeTermin.uhrzeitString").sendKeys(time);
        driver.findElementById("rueckgabeTermin.uhrzeitString").sendKeys(time);
        Select ausgabeOrte = new Select(driver.findElement(By.name("ausgabeTermin.ort")));
        ausgabeOrte.selectByVisibleText("Am Ende der Straße auf dem Parkplatz vom Supermarkt, Burgstraße 1");
        Select rueckgabeOrte = new Select(driver.findElement(By.name("rueckgabeTermin.ort")));
        rueckgabeOrte.selectByVisibleText("Am Ende der Straße rechts auf der Wiese, Burgstraße 12");
        driver.findElementById("requestBtn").click();
        assertTrue(driver.getCurrentUrl().contains("dashboard"));
    	Long erwarteteAnfragen = aktuelleAnfragen + 1;
        assertTrue(erwarteteAnfragen == anfrageDAO.count());
    }
    @Test
    public void createRequestForRentFailWhenOneDateIsMissing() throws ParseException {
    	DateTimeFormatter dtfDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	DateTimeFormatter dtfTime = DateTimeFormatter.ofPattern("HH:mm");
    	LocalDateTime now = LocalDateTime.now();
    	String dateNow = dtfDate.format(now);
    	String time = dtfTime.format(now);
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(sdf.parse(dateNow));
    	cal.add(Calendar.DATE,1);
    	String dateTomorrow = sdf.format(cal.getTime());
        HtmlUnitDriver driver = new HtmlUnitDriver();
        driver.get("http://localhost:"+port+"/login");
        driver.findElementById("Benutzername").sendKeys(ausleiher.getBenutzername());
        driver.findElementById("Passwort").sendKeys(ausleiher.getPasswort());
        driver.findElementById("btn1").click();
        assertEquals("Verleihportal 1.0 | Willkommen!", driver.getTitle());
        driver.get("http://localhost:"+port+"/artikel/"+artikel.getId());
        driver.findElementById("requestBtn").click();
        assertTrue(driver.getCurrentUrl().contains("ausleihanfrage/"+artikel.getId()));
        driver.findElementById("ausgabeTermin.datumString").sendKeys(dateTomorrow);
        driver.findElementById("ausgabeTermin.uhrzeitString").sendKeys(time);
        driver.findElementById("rueckgabeTermin.uhrzeitString").sendKeys(time);
        Select ausgabeOrte = new Select(driver.findElement(By.name("ausgabeTermin.ort")));
        ausgabeOrte.selectByVisibleText("Am Ende der Straße auf dem Parkplatz vom Supermarkt, Burgstraße 1");
        Select rueckgabeOrte = new Select(driver.findElement(By.name("rueckgabeTermin.ort")));
        rueckgabeOrte.selectByVisibleText("Am Ende der Straße rechts auf der Wiese, Burgstraße 12");
        driver.findElementById("requestBtn").click();
        assertTrue(driver.getCurrentUrl().contains("ausleihanfrage"));
    }


	private void loginAusleiher() {
		System.out.println("going to login ausleiher:"+ausleiher);
		//System.out.println("searching in DB for Benutzer.benutzername='ausleiher' gives:"+benutzerDAO.getBenutzerByBenutzername(ausleiher.getBenutzername()));
		driver.get("http://localhost:" + port + "/login");
		driver.findElementById("Benutzername").sendKeys(ausleiher.getBenutzername());
		driver.findElementById("Passwort").sendKeys(ausleiher.getPasswort());
		driver.findElementById("btn1").click();
	}


}
