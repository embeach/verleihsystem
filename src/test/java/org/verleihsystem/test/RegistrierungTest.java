package org.verleihsystem.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Hakim, tarik56
 * Tests für die 42-userstory-registrierung
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
@Commit
public class RegistrierungTest {

    /**
     * Port um das Verleihsystem zu testen
     */
    @LocalServerPort
    private int port;

    /**
     * HtmlUnitDriver
     */
    private HtmlUnitDriver driver = new HtmlUnitDriver();

    /**
     * Webelemente
     */
    private WebElement name;
    private WebElement email;
    private WebElement passwort;
    private WebElement passwortWdh;
    private WebElement submit;

    /**
     * Initialisiere Webelemente für das wiederholte Nutzen
     */
    @Before
    public void initialize() {
        driver.get("http://localhost:" + port + "/registrierung");
        this.name = driver.findElementById("bname");
        this.email = driver.findElementById("mail");
        this.passwort = driver.findElementById("password");
        this.passwortWdh = driver.findElementById("confirm_password");
        this.submit = driver.findElementById("submit");
    }

    /**
     * Teste ob die Weiterleitung richtig ist
     */
    @Test
    public void testForwardingToRegistration() {
        driver.get("http://localhost:" + port + "/login");
        driver.findElementById("btnRegistrierung").click();
        assertEquals(driver.getCurrentUrl(), "http://localhost:" + port + "/registrierung");
    }

    /**
     * Teste ob Pattern Message angezeigt wird bei zu kurzem Username
     */
    @Test
    public void testUsernameShort() {
        resetPatterns();
        name.sendKeys("short");
        email.sendKeys("testmail@mail.de");
        passwort.sendKeys("Passwort-12345");
        passwortWdh.sendKeys("Passwort-12345");
        submit.click();

        assertTrue(driver.findElementById("bnamePatternError") != null);
        try {
            assertTrue(driver.findElementById("emailPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdWdhError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
    }

    /**
     * Teste ob Pattern Message angezeigt wird bei zu langem Username
     */
    @Test
    public void testUsernameLong() {
        resetPatterns();
        name.sendKeys("thisIsAVeryLongUsername");
        email.sendKeys("testmail@mail.de");
        passwort.sendKeys("Passwort-12345");
        passwortWdh.sendKeys("Passwort-12345");
        submit.click();

        assertTrue(driver.findElementById("bnamePatternError") != null);
        try {
            assertTrue(driver.findElementById("emailPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdWdhError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
    }

    /**
     * Teste ob Pattern Message angezeigt wird bei der EMail ohne Domainendung
     */
    @Test
    public void testMailWithoutDomain() {
        resetPatterns();
        name.sendKeys("BenutzerName");
        email.sendKeys("testmail@mail");
        passwort.sendKeys("Passwort-12345");
        passwortWdh.sendKeys("Passwort-12345");
        submit.click();

        assertTrue(driver.findElementById("emailPatternError") != null);
        try {
            assertTrue(driver.findElementById("bnamePatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdWdhError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
    }

    /**
     * Teste ob Pattern Message angezeigt wird bei Email ohne "@"
     */
    @Test
    public void testMailWithoutAt() {
        resetPatterns();
        name.sendKeys("BenutzerName");
        email.sendKeys("testmail mail.de");
        passwort.sendKeys("Passwort-12345");
        passwortWdh.sendKeys("Passwort-12345");
        submit.click();

        assertTrue(driver.findElementById("emailPatternError") != null);
        try {
            assertTrue(driver.findElementById("bnamePatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdWdhError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
    }

    /**
     * Teste ob Pattern Message angezeigt wird bei zu kurzem Passwort
     */
    @Test
    public void testPasswordShort() {
        resetPatterns();
        name.sendKeys("BenutzerName");
        email.sendKeys("testmail@mail.de");
        passwort.sendKeys("Pass#1");
        passwortWdh.sendKeys("Pass#1");
        submit.click();

        assertTrue(driver.findElementById("pwdPatternError") != null);
        try {
            assertTrue(driver.findElementById("bnamePatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("emailPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdWdhError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
    }

    /**
     * Teste ob Pattern Message angezeigt wird bei Passwort ohne Großschreibung
     */
    @Test
    public void testPasswordLowCase() {
        resetPatterns();
        name.sendKeys("BenutzerName");
        email.sendKeys("testmail@mail.de");
        passwort.sendKeys("passwort-12345");
        passwortWdh.sendKeys("passwort-12345");
        submit.click();

        assertTrue(driver.findElementById("pwdPatternError") != null);
        try {
            assertTrue(driver.findElementById("bnamePatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("emailPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdWdhError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
    }

    /**
     * Teste ob Pattern Message angezeigt wird bei keinem Sonderzeichen
     */
    @Test
    public void testPasswordForSpecialChar() {
        resetPatterns();
        name.sendKeys("BenutzerName");
        email.sendKeys("testmail@mail.de");
        passwort.sendKeys("Passwort12345");
        passwortWdh.sendKeys("Passwort12345");
        submit.click();

        assertTrue(driver.findElementById("pwdPatternError") != null);
        try {
            assertTrue(driver.findElementById("bnamePatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("emailPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdWdhError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
    }

    /**
     * Teste ob Pattern Message angezeigt wird bei keiner Zahl
     */
    @Test
    public void testPasswordNoNumbers() {
        resetPatterns();
        name.sendKeys("BenutzerName");
        email.sendKeys("testmail@mail.de");
        passwort.sendKeys("Passwort-Passwort");
        passwortWdh.sendKeys("Passwort-Passwort");
        submit.click();

        assertTrue(driver.findElementById("pwdPatternError") != null);
        try {
            assertTrue(driver.findElementById("bnamePatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("emailPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdWdhError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
    }

    /**
     * Teste ob Pattern Message angezeigt wird bei unterschiedlichen Passwörtern
     */
    @Test
    public void testPasswordConfirmation() {
        resetPatterns();
        name.sendKeys("BenutzerName");
        email.sendKeys("testmail@mail.de");
        passwort.sendKeys("Passwort-12345");
        passwortWdh.sendKeys("Passwort-123455");
        submit.click();

        assertTrue(driver.findElementById("pwdWdhError") != null);
        try {
            assertTrue(driver.findElementById("bnamePatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("emailPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        try {
            assertTrue(driver.findElementById("pwdPatternError") == null);
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
    }

    /**
     * Teste ob die Weiterleitung und Registrierung erfolgreich ist
     */
    @Test
    public void testRegistrationSuccess() {
        name.sendKeys("Test2Test");
        email.sendKeys("testmail@mail.de");
        passwort.sendKeys("Passwort-12345");
        passwortWdh.sendKeys("Passwort-12345");
        submit.click();
        assertEquals(driver.getCurrentUrl(), "http://localhost:" + port + "/login");
    }


    /**
     * Entferne Clientseitige Pattern abfrage um die Serverseitige Patternabfrage prüfen
     * zu können
     */
    public void resetPatterns() {
        driver.setJavascriptEnabled(true);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('bname').setAttribute('pattern', '')");
        js.executeScript("document.getElementById('mail').setAttribute('pattern', '')");
        js.executeScript("document.getElementById('password').setAttribute('pattern', '')");
    }
}