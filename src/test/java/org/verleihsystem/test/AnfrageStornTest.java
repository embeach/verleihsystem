package org.verleihsystem.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.verleihsystem.dao.AnfrageDAO;
import org.verleihsystem.dao.ArtikelDAO;
import org.verleihsystem.dao.BenutzerDAO;
import org.verleihsystem.model.Anfrage;
import org.verleihsystem.model.Artikel;
import org.verleihsystem.model.Benutzer;
import org.verleihsystem.model.Status;

/**
 * @author Teddieh, Torben
 * JUnittest für das Anfrage-Stornierung
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AnfrageStornTest {

    @Autowired
    private AnfrageDAO anfrageDAO;

    @Autowired
    private ArtikelDAO artikelDAO;

    @Autowired
    private BenutzerDAO benutzerDAO;

    private Benutzer verleiher;

    private Benutzer ausleiher;

    private Artikel a;

    private Anfrage anf;

    /**
     * Datenbank Status herstellen
     */
    @Before
    public void before() {
        verleiher = TestDataFactory.createVerleiher();
        verleiher = benutzerDAO.save(verleiher);
        ausleiher = TestDataFactory.createAusleiher();
        ausleiher = benutzerDAO.save(ausleiher);
        a = TestDataFactory.createArtikel2(verleiher);
        a = artikelDAO.save(a);
        anf = TestDataFactory.createBestaetigteAnfrage(verleiher, ausleiher, a);
        anf.setStatus(Status.storniert);
        anf = anfrageDAO.save(anf);
    }

    /**
     * Testen ob die Anfrage storniert wurde
     */
    @Test
    public void anfrageStornieren() {
        Anfrage anfrage = anfrageDAO.findOne(anf.getId());
        Assert.assertEquals(Status.storniert, anfrage.getStatus());
    }
}
