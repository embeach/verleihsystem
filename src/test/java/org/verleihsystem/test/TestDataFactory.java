package org.verleihsystem.test;

import java.sql.Date;
import java.sql.Time;

import org.junit.Test;
import org.verleihsystem.model.Anfrage;
import org.verleihsystem.model.Artikel;
import org.verleihsystem.model.Artikelkategorie;
import org.verleihsystem.model.Artikelzustand;
import org.verleihsystem.model.Benutzer;
import org.verleihsystem.model.GebuehrArt;
import org.verleihsystem.model.Ort;
import org.verleihsystem.model.Pfand;
import org.verleihsystem.model.PfandArt;
import org.verleihsystem.model.Status;
import org.verleihsystem.model.Termin;
import org.verleihsystem.model.Verleihgebuehr;

/**
 * Utility Klasse um Testdaten zu erzeugen (um sie im nächsten Schritt zu persistieren)
 * @author embeach
 *
 */
public class TestDataFactory {
	/**
	 * ein test Verleiher Benutzer wird erstellt
	 * benutzername=verleiher
	 * passwort=pass1
	 * @author embeach
	 * @return verleiher
	 */
	public static Benutzer createVerleiher() {
		Benutzer verleiher = new Benutzer();
		verleiher.setBenutzername("verleiher");
		verleiher.setEmailAdresse("test1@test.com");
		verleiher.setPasswort("pass1");
		verleiher.setLetzteAktivitätDatum(new Date(5));
		verleiher.setLetzteAktivitätZeit(new Time(5));
		verleiher.setAngemeldet(false);
		return verleiher;
	}
	/**
	 * ein test Ausleiher Benutzer wird erstellt
	 * benutzername=ausleiher
	 * passwort=pass2
	 * @author embeach
	 * @return ausleiher
	 */
	public static Benutzer createAusleiher() {
		Benutzer ausleiher = new Benutzer();
		ausleiher.setBenutzername("ausleiher");
		ausleiher.setEmailAdresse("test2@test.com");
		ausleiher.setPasswort("pass2");
		ausleiher.setLetzteAktivitätDatum(new Date(5));
		ausleiher.setLetzteAktivitätZeit(new Time(5));
		ausleiher.setAngemeldet(false);
		return ausleiher;
		
	}
	/**
	 * eine test Artikelkategorie wird erstellt (Werkzeug)
	 * @author embeach
	 * @return artikelkategorie
	 */
	public static Artikelkategorie createArtikelkategorie() {
		Artikelkategorie artikelkategorie=new Artikelkategorie();
		artikelkategorie.setBezeichnung("Werkzeug");
		return artikelkategorie;
	}
	/**
	 * Ein test Artikel wird erstellt
	 * @param verleiher
	 * @param artikelkategorie
	 * @return artikel
	 */
	public static Artikel createArtikel(Benutzer verleiher, Artikelkategorie artikelkategorie) {
		Artikel artikel = new Artikel();
        artikel.setName("Bohrmaschine");
        artikel.setBeschreibung("Bohrmaschine mit Torx, Schlitz, Imbus Aufsätzen");
        artikel.setTechnischeDaten("1234,567");
        artikel.setKategorie(artikelkategorie);
        artikel.setVerleiher(verleiher);
        artikel.setZustand(Artikelzustand.wenigGebraucht);
        artikel.setPfand(createPfand(artikel));
        artikel.setGebuehr(createVerleihGebuehr(artikel));
        artikel.getUebergabeorte().add(createOrt1(artikel));
        artikel.getUebergabeorte().add(createOrt2(artikel));
		return artikel;
	}

	public static Artikel createArtikel2(Benutzer verleiher) {
		Artikel artikel = new Artikel();
		artikel.setName("Bohrmaschine");
		artikel.setBeschreibung("Bohrmaschine mit Torx, Schlitz, Imbus Aufsätzen");
		artikel.setTechnischeDaten("1234,567");
		artikel.setVerleiher(verleiher);
		artikel.setZustand(Artikelzustand.wenigGebraucht);
		artikel.setPfand(createPfand(artikel));
		artikel.setGebuehr(createVerleihGebuehr(artikel));
		artikel.getUebergabeorte().add(createOrt1(artikel));
		artikel.getUebergabeorte().add(createOrt2(artikel));
		return artikel;
	}


	/**
	 * ein test Pfand wird erstellt
	 * initialwerte und pfandArt=disabledPfandArt
	 * @param artikel
	 * @return pfand
	 */
	public static Pfand createPfand(Artikel artikel) {
		Pfand pfand = new Pfand();
		pfand.setArtikel(artikel);
		pfand.setPfandArt(PfandArt.disabledPfandArt);
		return pfand;
	}
	/**
	 * ein test Ort wird erstellt
	 * @param artikel
	 * @return ort
	 */
	public static Ort createOrt1(Artikel artikel) {
        Ort supermarkt = new Ort();
        supermarkt.setStrasse("Burgstraße");
        supermarkt.setStadt("Lübeck");
        supermarkt.setPlz("23562");
        supermarkt.setNummer("1");
        supermarkt.setBeschreibung("Am Ende der Straße auf dem Parkplatz vom Supermarkt");
        supermarkt.setArtikel(artikel);
		return supermarkt;
	}
	/**
	 * ein anderer test Ort wird erstellt
	 * @param artikel
	 * @return ort
	 */
	public static Ort createOrt2(Artikel artikel) {
        Ort wiese = new Ort();
        wiese.setStrasse("Burgstraße");
        wiese.setStadt("Lübeck");
        wiese.setPlz("23562");
        wiese.setNummer("12");
        wiese.setBeschreibung("Am Ende der Straße rechts auf der Wiese");
        wiese.setArtikel(artikel);
		return wiese;
	}

	/**
	 * eine test Verleihgebühr wird erstellt (0.0)
	 * @param a
	 * @return verleihgebuehr
	 */
	public static Verleihgebuehr createVerleihGebuehr(Artikel a){
		Verleihgebuehr vg = new Verleihgebuehr();
		vg.setArtikel(a);
		vg.setBetrag(0.0);
		vg.setGebuehrArt(GebuehrArt.keineGebuehr);
		return vg;
	}
	
	/**
	 * eine test Anfrage wird erstellt
	 * @param v verleiher der Anfrage
	 * @param a ausleiher der Anfrage
	 * @param ar artikel der Anfrage
	 * @return neue Anfrage
	 */
	public static Anfrage createGebuchteAnfrage(Benutzer v, Benutzer a, Artikel ar){
		Anfrage af = new Anfrage();
		af.setKommentar("kein Kommentar");
		af.setArtikel(ar);
		af.setRueckgabeTermin(TestDataFactory.createTermin());
		af.setAusgabeTermin(TestDataFactory.createTermin());
		af.setStatus(Status.gebucht);
		af.setVerleiher(v);
		af.setAusleiher(a);
		return af;
	}
	
	/**
	 * ein test Termin wird erstellt
	 * @author dschiko
	 * @return neuen Termin
	 */
	public static Termin createTermin(){
		Termin t = new Termin();
		t.setDatum(new Date(1L));
		t.setUhrzeit(new Time(1L));
		return t;
	}


	public static Anfrage createBestaetigteAnfrage(Benutzer v, Benutzer a, Artikel artikel) {
		Anfrage af = new Anfrage();
		af.setKommentar("Nein");
		af.setArtikel(artikel);
		af.setRueckgabeTermin(TestDataFactory.createTermin());
		af.setAusgabeTermin(TestDataFactory.createTermin());
		af.setStatus(Status.bestaetigt);
		af.setVerleiher(v);
		af.setAusleiher(a);
		return af;
	}
}
