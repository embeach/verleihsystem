package org.verleihsystem.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.verleihsystem.dao.AnfrageDAO;
import org.verleihsystem.dao.ArtikelDAO;
import org.verleihsystem.dao.BenutzerDAO;
import org.verleihsystem.dao.OrtDAO;
import org.verleihsystem.model.*;

import static org.junit.Assert.assertTrue;

/**
 * @author Teddieh, Torben, dschiko
 * Seleniumtests für das Anfrage-Stornierung
 * */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AnfrageStornierungTest {

    /**
     * Port um das Verleihsystem zu testen
     */
    @LocalServerPort
    private int port;

    /**
     * Alle ID's um die Elemente der View zu selektieren
     */
    private Artikel artikel;
    private Benutzer ausleiher;
    private Benutzer verleiher;
    private Anfrage anfrageBestaetigt;
    private Anfrage anfrageGebucht;

    /**
     * Alle 'data access objects' (DAO) für die Schnittstelle zur Datenbank
     */
    @Autowired
    private BenutzerDAO benutzerDAO;

    @Autowired
    private AnfrageDAO anfrageDAO;

    @Autowired
    private ArtikelDAO artikelDAO;

    @Autowired
    private OrtDAO ortDAO;

    /**
     * Erstellen des Kontexts vor jedem Test
     */
    @Before
    public void createContext() {
        this.deleteContext();
        verleiher = TestDataFactory.createVerleiher();
        benutzerDAO.save(verleiher);
        ausleiher = TestDataFactory.createAusleiher();
        benutzerDAO.save(ausleiher);
        this.artikel = TestDataFactory.createArtikel2(verleiher);
        this.artikel = artikelDAO.save(artikel);

        this.anfrageBestaetigt = TestDataFactory.createBestaetigteAnfrage(verleiher, ausleiher, artikel);
        Ort o = TestDataFactory.createOrt1(this.artikel);
        final Termin t = TestDataFactory.createTermin();
        o = this.ortDAO.save(o);
        t.setOrt(o);
        this.anfrageBestaetigt.setAusgabeTermin(t);
        this.anfrageBestaetigt.setRueckgabeTermin(t);
        this.anfrageBestaetigt = this.anfrageDAO.save(anfrageBestaetigt);

        this.anfrageGebucht = TestDataFactory.createGebuchteAnfrage(verleiher, ausleiher, artikel);
        Ort ortGebucht = TestDataFactory.createOrt1(this.artikel);
        final Termin t2 = TestDataFactory.createTermin();
        ortGebucht = this.ortDAO.save(ortGebucht);
        t2.setOrt(o);
        this.anfrageGebucht.setAusgabeTermin(t2);
        this.anfrageGebucht.setRueckgabeTermin(t2);
        this.anfrageGebucht = this.anfrageDAO.save(anfrageGebucht);

    }

    /**
     * Loeschen des Kontexts nach jedem Test
     */
    @After
    public void deleteContext() {
        artikelDAO.deleteAll();
        anfrageDAO.deleteAll();
        benutzerDAO.deleteAll();
    }

    /**
     * Anfrage wird storniert
     */
    @Test
    public void inquiryCancelSuccesful() {
        final HtmlUnitDriver driver = new HtmlUnitDriver();
        driver.get("http://localhost:" + port + "/login");
        driver.findElementById("Benutzername").sendKeys(ausleiher.getBenutzername());
        driver.findElementById("Passwort").sendKeys(ausleiher.getPasswort());
        driver.findElementById("btn1").click();
        final Long id = this.anfrageBestaetigt.getId();
        driver.findElement(By.xpath("//a[@href='/anfrage/" + id + "']")).click();
        driver.findElementById("cancel").click();
        driver.findElementById("stornieren").click();
        assertTrue(driver.getCurrentUrl().contains("/dashboard?stornieren=true"));
    }

    /**
     * Anfrage wird nicht storniert
     */
    @Test
    public void inquiryCancelUnsuccesful() {
        final HtmlUnitDriver driver = new HtmlUnitDriver();
        driver.get("http://localhost:" + port + "/login");
        driver.findElementById("Benutzername").sendKeys(ausleiher.getBenutzername());
        driver.findElementById("Passwort").sendKeys(ausleiher.getPasswort());
        driver.findElementById("btn1").click();
        final Long id = this.anfrageGebucht.getId();
        driver.findElement(By.xpath("//a[@href='/anfrage/" + id + "']")).click();
        driver.findElementById("btnCancel").click();
        driver.findElementById("btnCancel2").click();
        assertTrue(driver.getCurrentUrl().contains("/dashboard?stornieren=false"));
    }
}
