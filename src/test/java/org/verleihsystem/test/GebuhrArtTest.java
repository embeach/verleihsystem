package org.verleihsystem.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.verleihsystem.model.GebuehrArt;

/**
 * @author dschiko
 * Tests für die Gebuehrart
 */
@RunWith(SpringRunner.class)
public class GebuhrArtTest {
	
	/**
	 * @author dschiko, embeach
	 * Es gibt eine feste Anzahl an Frequenzen um Gebuehren zu zahlen (frequenz=Bezugsintervall für zeitliche Verleihgebühr)
	 */
	@Test
	public void getFrequenzen_Length4(){
		final GebuehrArt [] frequenzen = GebuehrArt.getFrequenzen();
		assert(frequenzen.length == 4);
	}
	
	/**
	 * createFromString: valide Beschreibung als Eingabe
	 * @author embeach
	 */
	@Test
	public void createFromString_validBeschr() {
		assert(GebuehrArt.createFromString("täglich")==GebuehrArt.taeglich);
	}
	/**
	 * createFromString: invalide Beschreibung als Eingabe
	 * @author embeach
	 */
	@Test
	public void createFromString_invalidBeschr() {
		assert(GebuehrArt.createFromString("fehlerhaftes")==null);
	}
	/**
	 * createFromString: deprecated Beschreibung als Eingabe
	 * @author embeach
	 */
	@Test
	public void createFromString_deprecatedBeschr() {
		assert(GebuehrArt.createFromString("keine Gebühr")==null);
	}
	
}
