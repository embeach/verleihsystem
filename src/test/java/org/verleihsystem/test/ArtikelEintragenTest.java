package org.verleihsystem.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.sql.Time;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;
import org.verleihsystem.dao.ArtikelDAO;
import org.verleihsystem.dao.ArtikelkategorieDAO;
import org.verleihsystem.dao.BenutzerDAO;
import org.verleihsystem.model.Artikelkategorie;
import org.verleihsystem.model.Artikelzustand;
import org.verleihsystem.model.Benutzer;

/**
 * @author dschiko, embeach
 * Tests für das Artikel-Eintragen-Formular
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ArtikelEintragenTest {
    /**
     * Port um das Verleihsystem zu testen
     *
     * @author dschiko
     */
    @LocalServerPort
    private int port;
    /**
     * Alle ID's um die Elemente der View zu selektieren
     *
     * @author dschiko
     */
    private final String name = "artikelname";
    private final String description = "artikelbeschreibung";
    private final String dataSheet = "technischeDaten";
    private final String category = "artikelKategorie";
    private final String state = "artikelZustand";
    private final String handOverPlaces = "uebergabeOrte";
    private final String handoverLocationStreet = "uebergabeort-str";
    private final String handoverLocationNr = "uebergabeort-nr";
    private final String handoverLocationPostCode = "uebergabeort-plz";
    private final String handoverLocationCity = "uebergabeort-stadt";
    private final String handoverLocationDescription = "uebergabeort-beschreibung";
    private final String securityInEuro = "pfandInEuro";
    private final String securitySum = "pfandBetrag";
    private final String securityDocument = "pfandGegenstand";
    private final String securityChargeInEuro = "verleihgebuehrInEuro";
    private final String fixedCharge = "festeGebuehr";
    private final String timedCharge = "zeitlicheGebuehr";
    private final String chargeOptions = "frequenzen";
    private final String dailyOption = "frequenz2";
    private final String doneButton = "fertigStellenBtn";
    private final String newArticleMessage = "neuerArtikelNachricht";

    /**
     * Alle 'data access objects' (DAO) für die Schnittstelle zur Datenbank
     *
     * @author dschiko
     */
    @Autowired
    private BenutzerDAO benutzerDAO;
    @Autowired
    private ArtikelkategorieDAO artikelKategorieDAO;
    @Autowired
    private ArtikelDAO artikelDAO;

    /**
     * Daten zur Anmeldung eines Test-Benutzers
     *
     * @author dschiko
     */
    private final String username = "verleiher";
    private final String password = "pass";
    /**
     * Schnittstelle um mit dem Artikel-Eintragen-Formular zu interagieren
     *
     * @author dschiko
     */
    private HtmlUnitDriver driver = new HtmlUnitDriver();

    /**
     * Erstellen des Kontexts vor jedem Test
     *
     * @author dschiko
     */
    @Before
    public void before() {
        this.deleteContext();
        this.createUser();
        this.loginUser();
    }

    /**
     * Loeschen des Kontexts nach jedem Test
     *
     * @author dschiko
     */
    @After
    public void after() {
        this.deleteContext();
    }

    /**
     * Der Benutzer navigiert zum Artikel-Eintragen-Formular
     *
     * @throws Exception Interaktion mit dem Artikel-Eintragen-Formular fehlgeschlagen
     * @author dschiko
     */
    @Test
    public void user_click_link_to_navigate_to_add_article_site() throws Exception {
        assertTrue(this.driver.getCurrentUrl().contains("dashboard"));
        navigateToFormularToCreateArticle();
    }

    /**
     * Der Benutzer sieht das leere Artikel-Eintragen-Formular
     *
     * @throws Exception Interaktion mit dem Artikel-Eintragen-Formular fehlgeschlagen
     * @author dschiko
     */
    @Test
    public void user_sees_empty_formular_to_add_an_article() throws Exception {
        navigateToFormularToCreateArticle();
        final boolean formularExists = this.driver.findElementById("headline") != null
                && this.driver.findElementById("artikelEintragen") != null
                && this.driver.findElementById(this.name) != null
                && this.driver.findElementById(this.description) != null
                && this.driver.findElementById(this.dataSheet) != null
                && this.driver.findElementById(this.category) != null
                && this.driver.findElementById(this.state) != null
                && this.driver.findElementById(this.handOverPlaces) != null
                && this.driver.findElementById(this.securityInEuro) != null
                && this.driver.findElementById(this.securitySum) != null
                && this.driver.findElementById(this.securityDocument) != null
                && this.driver.findElementById(this.securityChargeInEuro) != null
                && this.driver.findElementById(this.fixedCharge) != null
                && this.driver.findElementById(this.timedCharge) != null
                && this.driver.findElementById(this.chargeOptions) != null
                && this.driver.findElementById(this.doneButton) != null;

        assertTrue(formularExists);
    }

    /**
     * Der Benutzer sieht den neuen Artikel in der Artikel-Detailansicht nach erfolgreicher Erstellung
     *
     * @throws Exception Interaktion mit dem Artikel-Eintragen-Formular fehlgeschlagen
     * @author dschiko
     */
    @Test
    public void user_sees_created_article_on_article_site() throws Exception {
        final Artikelkategorie ak = this.createOneCategoryContext();
        this.navigateToFormularToCreateArticle();
        this.fillInNameAndDescriptionAndDatasheet();
        final Select category = new Select(driver.findElement(By.id(this.category)));
        category.selectByVisibleText(ak.getBezeichnung());
        final Select state = new Select(driver.findElement(By.id(this.state)));
        state.selectByVisibleText(Artikelzustand.wieNeu.getBeschreibung());
        this.fillInOneHandOverLocation();
        this.driver.findElementById(this.securitySum).sendKeys("0");
        this.driver.findElementById(this.securityDocument).sendKeys("");
        this.driver.findElementById(this.fixedCharge).sendKeys("0");
        this.driver.findElementById(this.timedCharge).sendKeys("0");
        this.driver.findElementById(this.dailyOption).click();
        this.driver.findElementById(this.doneButton).click();
        assertTrue(this.driver.getCurrentUrl().matches(".*/artikel/[0-9]+.*"));
    }

    /**
     * Der Benutzer erstellt einen neuen Artikel mit einem Pfandbetrag
     *
     * @throws Exception Interaktion mit dem Artikel-Eintragen-Formular fehlgeschlagen
     * @author dschiko
     */
    @Test
    public void user_creates_one_article_with_security_sum() throws Exception {
        final String securitySum = "100";
        final Artikelkategorie ak = this.createOneCategoryContext();
        this.navigateToFormularToCreateArticle();
        this.fillInNameAndDescriptionAndDatasheet();
        final Select category = new Select(driver.findElement(By.id(this.category)));
        category.selectByVisibleText(ak.getBezeichnung());
        final Select state = new Select(driver.findElement(By.id(this.state)));
        state.selectByVisibleText(Artikelzustand.wieNeu.getBeschreibung());
        this.fillInOneHandOverLocation();
        this.driver.findElementById(this.securitySum).clear();
        this.driver.findElementById(this.securitySum).sendKeys(securitySum);
        this.driver.findElementById(this.securityDocument).sendKeys("");
        this.driver.findElementById(this.fixedCharge).sendKeys("0");
        this.driver.findElementById(this.timedCharge).sendKeys("0");
        this.driver.findElementById(this.dailyOption).click();
        this.driver.findElementById(this.doneButton).click();
        assertTrue(this.driver.getCurrentUrl().matches(".*/artikel/[0-9]+.*"));
        assertTrue(this.driver.findElementByXPath("//td[contains(text(),'" + securitySum + "')]") != null);
    }


    /**
     * Der Benutzer erstellt einen neuen Artikel mit einem Pfandgegenstand
     *
     * @throws Exception Interaktion mit dem Artikel-Eintragen-Formular fehlgeschlagen
     * @author dschiko
     */
    @Test
    public void user_creates_one_article_with_security_document() throws Exception {
        final String securityDocument = "Personalausweis";
        final Artikelkategorie ak = this.createOneCategoryContext();
        this.navigateToFormularToCreateArticle();
        this.fillInNameAndDescriptionAndDatasheet();
        final Select category = new Select(driver.findElement(By.id(this.category)));
        category.selectByVisibleText(ak.getBezeichnung());
        final Select state = new Select(driver.findElement(By.id(this.state)));
        state.selectByVisibleText(Artikelzustand.wieNeu.getBeschreibung());
        this.fillInOneHandOverLocation();
        this.driver.findElementById(this.securitySum).sendKeys("0");
        this.driver.findElementById(this.securityDocument).sendKeys(securityDocument);
        this.driver.findElementById(this.fixedCharge).sendKeys("0");
        this.driver.findElementById(this.timedCharge).sendKeys("0");
        this.driver.findElementById(this.dailyOption).click();
        this.driver.findElementById(this.doneButton).click();
        assertTrue(this.driver.getCurrentUrl().matches(".*/artikel/[0-9]+.*"));
        assertTrue(this.driver.findElementByXPath("//td[contains(text(),'" + securityDocument + "')]") != null);
    }

    /**
     * Der Benutzer erstellt einen neuen Artikel mit einer festen Verleihgebuehr
     *
     * @throws Exception Interaktion mit dem Artikel-Eintragen-Formular fehlgeschlagen
     * @author dschiko
     */
    @Test
    public void user_creates_one_article_with_fixed_charge() throws Exception {
        final String fixedCharge = "10000";
        final Artikelkategorie ak = this.createOneCategoryContext();
        this.navigateToFormularToCreateArticle();
        this.fillInNameAndDescriptionAndDatasheet();
        final Select category = new Select(driver.findElement(By.id(this.category)));
        category.selectByVisibleText(ak.getBezeichnung());
        final Select state = new Select(driver.findElement(By.id(this.state)));
        state.selectByVisibleText(Artikelzustand.wieNeu.getBeschreibung());
        this.fillInOneHandOverLocation();
        this.driver.findElementById(this.securitySum).sendKeys("0");
        this.driver.findElementById(this.securityDocument).sendKeys("");
        this.driver.findElementById(this.fixedCharge).clear();
        this.driver.findElementById(this.fixedCharge).sendKeys(fixedCharge);
        this.driver.findElementById(this.timedCharge).sendKeys("0");
        this.driver.findElementById(this.dailyOption).click();
        this.driver.findElementById(this.doneButton).click();
        assertTrue(this.driver.getCurrentUrl().matches(".*/artikel/[0-9]+.*"));
        assertTrue(this.driver.findElementByXPath("//td[contains(text(),'" + fixedCharge + "')]") != null);
    }

    /**
     * Der Benutzer erstellt einen neuen Artikel mit einer zeitlichen Verleihgebuehr
     *
     * @throws Exception Interaktion mit dem Artikel-Eintragen-Formular fehlgeschlagen
     * @author dschiko
     */
    @Test
    public void user_creates_one_article_with_timed_charge() throws Exception {
        final String timedCharge = "100";
        final Artikelkategorie ak = this.createOneCategoryContext();
        this.navigateToFormularToCreateArticle();
        this.fillInNameAndDescriptionAndDatasheet();
        final Select category = new Select(driver.findElement(By.id(this.category)));
        category.selectByVisibleText(ak.getBezeichnung());
        final Select state = new Select(driver.findElement(By.id(this.state)));
        state.selectByVisibleText(Artikelzustand.wieNeu.getBeschreibung());
        this.fillInOneHandOverLocation();
        this.driver.findElementById(this.securitySum).sendKeys("0");
        this.driver.findElementById(this.securityDocument).sendKeys("");
        this.driver.findElementById(this.fixedCharge).sendKeys("");
        this.driver.findElementById(this.timedCharge).clear();
        this.driver.findElementById(this.timedCharge).sendKeys(timedCharge);
        this.driver.findElementById(this.dailyOption).click();
        this.driver.findElementById(this.doneButton).click();
        assertTrue(this.driver.getCurrentUrl().matches(".*/artikel/[0-9]+.*"));
        assertTrue(this.driver.findElementByXPath("//td[contains(text(),'" + timedCharge + "')]") != null);
    }

    /**
     * Der Benutzer sieht eine Nachricht wenn der Artikel erfolgreich hinzugefuegt wurde
     *
     * @throws Exception Interaktion mit dem Artikel-Eintragen-Formular fehlgeschlagen
     * @author dschiko
     */
    @Test
    public void user_sees_message_when_article_is_created_successfully() throws Exception {
        final Artikelkategorie ak = this.createOneCategoryContext();
        this.navigateToFormularToCreateArticle();
        this.fillInNameAndDescriptionAndDatasheet();
        final Select category = new Select(driver.findElement(By.id(this.category)));
        category.selectByVisibleText(ak.getBezeichnung());
        final Select state = new Select(driver.findElement(By.id(this.state)));
        state.selectByVisibleText(Artikelzustand.wieNeu.getBeschreibung());
        this.fillInOneHandOverLocation();
        this.driver.findElementById(this.securitySum).sendKeys("0");
        this.driver.findElementById(this.securityDocument).sendKeys("");
        this.driver.findElementById(this.fixedCharge).sendKeys("");
        this.driver.findElementById(this.timedCharge).clear();
        this.driver.findElementById(this.timedCharge).sendKeys("0");
        this.driver.findElementById(this.dailyOption).click();
        this.driver.findElementById(this.doneButton).click();
        assertTrue(this.driver.getCurrentUrl().matches(".*/artikel/[0-9]+.*"));
        assertTrue(this.driver.findElementById(this.newArticleMessage) != null);
    }

    /**
     * Der Benutzer erstellt einen neuen Artikel ohne einen Namen (d.h. ohne Überschrift)
     *
     * @throws Exception Interaktion mit dem Artikel-Eintragen-Formular fehlgeschlagen
     * @author embeach
     */
    @Test

    public void user_submits_article_formular_without_article_headline() {
        navigateToFormularToCreateArticle();
        String url_artikelFormular = driver.getCurrentUrl();
        try {
            driver.findElementById("artikelname_error");
        } catch (Exception e) {
            assertTrue(e instanceof NoSuchElementException);
        }
        this.driver.findElementById(this.doneButton).click();
        assertEquals(driver.getCurrentUrl(), url_artikelFormular);
        assertNotNull(this.driver.findElementById("artikelname_error"));
    }

    /**
     * Der Benutzer navigiert zum Artikel-Eintragen-Formular
     *
     * @author dschiko
     */
    private void navigateToFormularToCreateArticle() {
        this.driver.findElement(By.linkText("Weiteren Artikel hinzufügen")).click();
        assertTrue(this.driver.getCurrentUrl().matches(".*/artikelEintragen"));
    }

    /**
     * Aktuellen Kontext fuer den naechsten Test loeschen
     *
     * @author dschiko
     */
    private void deleteContext() {
        this.benutzerDAO.deleteAll();
        this.artikelKategorieDAO.deleteAll();
        this.artikelDAO.deleteAll();
    }

    /**
     * Einen Test-Benutzer erstellen
     *
     * @author dschiko
     */
    private void createUser() {
        Benutzer verleiher = new Benutzer();
        verleiher.setBenutzername(this.username);
        verleiher.setEmailAdresse("test@test.com");
        verleiher.setPasswort(this.password);
        verleiher.setLetzteAktivitätDatum(new Date(5));
        verleiher.setLetzteAktivitätZeit(new Time(5));
        verleiher.setAngemeldet(false);
        this.benutzerDAO.save(verleiher);
    }

    /**
     * Einen Test-Benutzer anmelden
     *
     * @author dschiko
     */
    private void loginUser() {
        this.driver.get("http://localhost:" + port + "/login");
        this.driver.findElementById("Benutzername").sendKeys(this.username);
        this.driver.findElementById("Passwort").sendKeys(this.password);
        this.driver.findElementById("btn1").click();
    }

    /**
     * Die Beschreibung, Namen und die Technischendaten fuer einen Artikel eintragen
     *
     * @author dschiko
     */
    private void fillInNameAndDescriptionAndDatasheet() {
        this.driver.findElementById(this.name).sendKeys("Hilty");
        this.driver.findElementById(this.description).sendKeys("Bohrmaschine");
        this.driver.findElementById(this.dataSheet).sendKeys("240 Volt, 10mm Bohrer");
    }

    /**
     * Eine Artikelkategorie erstellen
     *
     * @return Erstellte Artikelkategorie
     * @author dschiko
     */
    private Artikelkategorie createOneCategoryContext() {
        Artikelkategorie ak = new Artikelkategorie();
        ak.setBezeichnung("Werkzeug");
        return this.artikelKategorieDAO.save(ak);
    }


    /**
     * Einen Uebergabeort erstellen
     *
     * @author dschiko
     */
    private void fillInOneHandOverLocation() {
        this.driver.findElementById(this.handoverLocationStreet).sendKeys("Musterstraße");
        this.driver.findElementById(this.handoverLocationNr).sendKeys("1");
        this.driver.findElementById(this.handoverLocationPostCode).sendKeys("12345");
        this.driver.findElementById(this.handoverLocationCity).sendKeys("Musterburg");
        this.driver.findElementById(this.handoverLocationDescription).sendKeys("Bei der Bushaltestelle");
    }
}
