package org.verleihsystem.test;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.verleihsystem.model.Artikel;

/**
 * @author dschiko
 * Tests fuer Artikel
 */
@RunWith(SpringRunner.class)
public class ArtikelTest {
	
	/**
	 * @author dschiko,embeach
	 * Testet das Erstellen eines leeren Artikels
	 */
	@Test
	public void createEmptyArtikel_someFieldsNotNull_someFieldsBlank_someEmpty(){
		final Artikel a = Artikel.createEmptyArtikel();
		assert(a.getPfand() != null);
		assert(a.getGebuehr() != null);
		assert(a.getKategorie() != null);
		assert(a.getName()=="");
		assert(a.getBeschreibung()=="");
		assert(a.getTechnischeDaten()=="");
		assert(a.getUebergabeorte().isEmpty());
		assert(a.getBilder().isEmpty());
	}
}
