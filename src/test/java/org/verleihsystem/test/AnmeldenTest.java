package org.verleihsystem.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.verleihsystem.dao.BenutzerDAO;
import org.verleihsystem.model.Benutzer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AnmeldenTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private BenutzerDAO benutzerDAO;

    private Benutzer verleiher;

    @Before
    public void createContext() {
        benutzerDAO.deleteAll();
        verleiher = TestDataFactory.createVerleiher();
        benutzerDAO.save(verleiher);
    }

    @After
    public void deleteContext() {
        benutzerDAO.deleteAll();
    }

    @Test
    public void greetingShouldReturnDefaultMessage() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/login",
                String.class)).contains("Verleihsystem");
    }

    @Test
    public void loginUserSuccess() {
        HtmlUnitDriver driver = new HtmlUnitDriver();
        driver.get("http://localhost:" + port + "/login");
        driver.findElementById("Benutzername").sendKeys(verleiher.getBenutzername());
        driver.findElementById("Passwort").sendKeys(verleiher.getPasswort());
        driver.findElementById("btn1").click();
        assertEquals("Verleihportal 1.0 | Willkommen!", driver.getTitle());
    }

    @Test
    public void loginUserFail() {
        HtmlUnitDriver driver = new HtmlUnitDriver();
        driver.get("http://localhost:" + port + "/login");
        driver.findElementById("Benutzername").sendKeys(verleiher.getBenutzername());
        driver.findElementById("Passwort").sendKeys("wrongPass");
        driver.findElementById("btn1").click();
        assertEquals("Login", driver.getTitle());
    }
}
