package org.verleihsystem.test;
import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Date;
import java.sql.Time;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.verleihsystem.Application;
import org.verleihsystem.dao.BenutzerDAO;
import org.verleihsystem.model.Benutzer;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = Application.class)
public class AnmeldenJPATest {

	@Autowired
	private BenutzerDAO benutzerDAO;

	private Benutzer user;
	
	@Before
	public void before(){
		user = new Benutzer();
		user.setBenutzername("max");
		user.setEmailAdresse("test@test.com");
		user.setPasswort("1234");
		user.setLetzteAktivitätDatum(new Date(1L));//TODO evt echtes Datum nehmen
		user.setLetzteAktivitätZeit(new Time(1L));//TODO evt echtes Datum nehmen
	}
	
	@Test
	public void angemeldet() throws Exception {
		user.setAngemeldet(true);
		Benutzer user_=benutzerDAO.save(user);
		Benutzer expectedUser = benutzerDAO.findOne(user_.getId());
		assertThat(expectedUser.getBenutzername()).isEqualTo(this.user.getBenutzername());
		assertThat(expectedUser.getPasswort()).isEqualTo(this.user.getPasswort());
		assertThat(expectedUser.isAngemeldet()).isEqualTo(true);
	}
	@Test
	public void nicht_angemeldet() throws Exception {
		user.setAngemeldet(false);
		Benutzer user_=benutzerDAO.save(user);
		Benutzer expectedUser = benutzerDAO.findOne(user_.getId());
		assertThat(expectedUser.getBenutzername()).isEqualTo(this.user.getBenutzername());
		assertThat(expectedUser.getPasswort()).isEqualTo(this.user.getPasswort());
		assertThat(expectedUser.isAngemeldet()).isEqualTo(false);
	}
}
