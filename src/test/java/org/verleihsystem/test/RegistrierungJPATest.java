package org.verleihsystem.test;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.verleihsystem.Application;
import org.verleihsystem.dao.BenutzerDAO;
import org.verleihsystem.model.Benutzer;
import java.sql.Date;
import java.sql.Time;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = Application.class)
public class RegistrierungJPATest {

    @Autowired
    private BenutzerDAO benutzerDAO;

    @Test
    public void testRegister() {
        Benutzer newUser = new Benutzer();

        newUser.setBenutzername("testUser");
        newUser.setEmailAdresse("test@test.de");
        newUser.setPasswort("Password-12345");
        newUser.setLetzteAktivitätDatum(new Date(5));
        newUser.setLetzteAktivitätZeit(new Time(5));
        benutzerDAO.save(newUser);

        Assert.assertEquals("testUser", newUser.getBenutzername());
        Assert.assertEquals("test@test.de", newUser.getEmailAdresse());
        Assert.assertEquals("Password-12345", newUser.getPasswort());
    }
}