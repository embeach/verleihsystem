package org.verleihsystem.viewmodel;



import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.verleihsystem.model.Artikel;


/**
 * @author dschiko,embeach
 * Viewmodel für das Artikel-Eintragen-Formular
 */
public class ArtikelForm {
	/**
	 * Neuer Artikel im Verleihsystem
	 * @author dschiko
	 */
	@Valid
	private Artikel artikel;
	/**
	 * Moeglichkeit eine Gebuehr zu zahlen z.B. teaglich
	 * @author dschiko,embeach
	 */
	@NotBlank(message="Bitte​ ​Intervall für zeitliche Gebühr​ wählen (F21)",groups= {ArtikelForm.FormValidation.class})
	private String frequenz;

	/**
	 * @author dschiko
	 * @return Neuer Artikel im Verleihsystem
	 */
	public Artikel getArtikel() {
		return artikel;
	}


	/**
	 * @author dschiko
	 * @param artikel Neuer Artikel im Verleihsystem
	 */
	public void setArtikel(Artikel artikel) {
		this.artikel = artikel;
	}

	/**
	 * @author dschiko
	 * @return Moeglichkeit eine Gebuehr zu zahlen z.B. teaglich
	 */
	public String getFrequenz() {
		return frequenz;
	}

	/**
	 * @author dschiko
	 * @param f Moeglichkeit eine Gebuehr zu zahlen z.B. teaglich
	 */
	public void setFrequenz(String f) {
		this.frequenz = f;
	}
	
	/**
	 * Validierungsgruppe für ArtikelFormValidation
	 * @author embeach
	 *
	 */
	public interface FormValidation{}
}
