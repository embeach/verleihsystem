package org.verleihsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.verleihsystem.dao.AnfrageDAO;
import org.verleihsystem.dao.ArtikelDAO;
import org.verleihsystem.model.Anfrage;
import org.verleihsystem.model.Artikel;
import org.verleihsystem.model.Benutzer;
import org.verleihsystem.model.Status;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.sql.Time;

/**@Teddieh, Torben
 * Controller für saemtliche Endpunkte im Zusammenhang mit Artikeln
 */
@Controller
public class AnfrageController extends WebMvcConfigurerAdapter {
    /**
     *Fehlermeldung bei nicht Einhalten der Bedingungen
     */
    private static final String F23 = "Anfangsdatum muss vor Rueckgabedatum liegen";
    private static final String F24 = "Zeitraum befindet sich vor aktuellem Datum";
    private static final String F25 = "Uebergabe-Datum muss angegeben werden";
    private static final String F26 = "Uebergabe-Uhrzeit muss angegeben werden";
    private static final String F27 = "Uebergabeort muss aus der Liste gewaehlt werden";

    /**
     * Die Schnittstelle der Datenbank von Anfragen
     */
    @Autowired
    private AnfrageDAO anfrageDAO;
    /**
     * Die Schnittstelle der Datenbank von Artikeln
     */
    @Autowired
    private ArtikelDAO artikelDAO;

    /**
     * Der Anfragezustand wird auf storniert gesetzt
     * @param id Die Anfrage ID
     * @param session Session für den angemeldeten Benutzer. Darf nicht null sein.
     * @return Benutzer ist nicht angemeldet
     * @return Anfrage nicht storniert
     * @return Anfrage wurde erfolgreich storniert
     */
    @GetMapping("/stornieren/{anfrageId}")
    public String stornieren(@PathVariable("anfrageId") long id, HttpSession session) {
        Benutzer user = (Benutzer) session.getAttribute("benutzer");
        if (user == null || !user.isAngemeldet()) return "redirect:/login";
        Anfrage anfrage = anfrageDAO.findOne(id);
        if (!user.getGesendeteAnfragen().contains(anfrage) || !(anfrage.getStatus().equals(Status.bestaetigt))) {
            return "redirect:/dashboard?stornieren="+false;
        }
        anfrage.setStatus(Status.storniert);
        anfrageDAO.save(anfrage);
        // Anfrage wurde erfolreich storniert
        return "redirect:/dashboard?stornieren=" + true;
    }

    /**
     * Der Anfragezustand wird aufgerufen und wird auf Buchen gesetzt
     * @param id Die AnfrageId
     * @param session Session für den angemeldeten Benutzer. Darf nicht null sein.
     * @return Benutzer ist nicht angemeldet. Man wird auf das Loginseite geleitet
     * @return gesendetete Anfrage existiert nicht und man wird auf die Dashboardseite weitergeleitet
     * @return Anfrage wurde gebucht
     */
    @GetMapping("/buchen/{anfrageId}")
    public String buchen(@PathVariable("anfrageId") long id, HttpSession session) {
        Benutzer user = (Benutzer) session.getAttribute("benutzer");
        if (user == null || !user.isAngemeldet()) return "redirect:/login";
        Anfrage anfrage = anfrageDAO.findOne(id);
        if (!user.getGesendeteAnfragen().contains(anfrage)) return "redirect:/dashboard";
        anfrage.setStatus(Status.gebucht);
        anfrageDAO.save(anfrage);
        return "redirect:/dashboard";
    }

    /**
     * Der Anfragezustand wird verändert oder aktualisiert und auf bestätigt gesetzt
     * @param id die AnfrageID
     * @param session Session für den angemeldeten Benutzer. Darf nicht null sein.
     * @param anfrage Anfrage ein Kommentar zu der Anfrage.
     * @return Benutzer ist nicht angemeldet. Man wird auf die Loginseite geleitet
     * @return Die empfangene Anfragen existiert nicht. Man wird auf die Dashboardseite geleitet
     * @return Anfrage wird bestätigt. Man wird auf die Dashboardseite geleitet
     */
    @PostMapping("/bestaetigen/{anfrageId}")
    public String bestaetigen(@PathVariable("anfrageId") long id, HttpSession session,
                              @ModelAttribute Anfrage anfrage) {
        Benutzer user = (Benutzer) session.getAttribute("benutzer");
        if (user == null || !user.isAngemeldet())
            return "redirect:/login";
        Anfrage dbAnfrage = anfrageDAO.findOne(id);
        if (!user.getEmpfangeneAnfragen().contains(dbAnfrage))
            return "redirect:/dashboard";
        dbAnfrage.setStatus(Status.bestaetigt);
        dbAnfrage.setKommentar(anfrage.getKommentar());
        anfrageDAO.save(dbAnfrage);
        return "redirect:/dashboard";
    }

    /**
     *Der Anfragezustand wird aufgerufen und auf bestätigt gesetzt
     * @param id die AnfrageID
     * @param session Session für den angemeldeten Benutzer. Darf nicht null sein.
     * @return Benutzer ist nicht angemeldet. Man wird auf die Loginseite geleitet
     * @return Die empfangene Anfragen existiert nicht. Man wird auf die Dashboardseite geleitet
     * @return Anfrage wird bestätigt. Man wird auf die Dashboardseite geleitet
     */
    @GetMapping("/bestaetigen/{anfrageId}")
    public String bestaetigen(@PathVariable("anfrageId") long id, HttpSession session) {
        Benutzer user = (Benutzer) session.getAttribute("benutzer");
        if (user == null || !user.isAngemeldet())
            return "redirect:/login";
        Anfrage anfrage = anfrageDAO.findOne(id);
        if (!user.getEmpfangeneAnfragen().contains(anfrage))
            return "redirect:/dashboard";
        anfrage.setStatus(Status.bestaetigt);
        anfrageDAO.save(anfrage);
        return "redirect:/dashboard";
    }

    /**
     * Die Anfragezustand der Anfragedeteils wird aufgerufen
     * @param id Anfrage ID
     * @param session Session für den angemeldeten Benutzer. Darf nicht null sein.
     * @param model Attribut wird hinzugefügt
     * @return Benutzer ist nicht angemeldet. Man wird auf die Loginseite geleitet
     * @return Die empfangene Anfragen existiert nicht. Man wird auf die Dashboardseite geleitet
     * @return Die HMTL-Buchen wird aufgerufen
     */
    @GetMapping("/anfrage/{anfrageId}")
    public String Anfragedetails(@PathVariable("anfrageId") long id, HttpSession session, Model model) {
        Benutzer user = (Benutzer) session.getAttribute("benutzer");
        if (user == null || !user.isAngemeldet())
            return "redirect:/login";
        Anfrage anfrage = anfrageDAO.findOne(id);
        if (user.getEmpfangeneAnfragen().contains(anfrage)) {
            model.addAttribute("verleiher", true);
        }
        if (user.getGesendeteAnfragen().contains(anfrage)) {
            model.addAttribute("ausleiher", true);
        } else if (!user.getEmpfangeneAnfragen().contains(anfrage)) {
            return "redirect:/dashboard";
        }
        model.addAttribute("anfrage", anfrage);
        return "buchen";
    }

    /**
     * Der Zustand von Anfrage erstellen wird erstellt.
     * @param id Anfrage ID
     * @param session Session für den angemeldeten Benutzer. Darf nicht null sein.
     * @param anfrage die Anfrage wird mit Daten gesetzt
     * @param model Attribut wird hinzugefügt
     * @return Benutzer ist nicht angemeldet. Man wird auf die Loginseite geleitet
     * @return ausleihanfrage wurde erstellt
     */
    @PostMapping("/ausleihanfrage/{artikelId}")
    public String anfrageErstellen(@PathVariable("artikelId") long id, HttpSession session,
                                   @ModelAttribute Anfrage anfrage, Model model) {
        Benutzer user = (Benutzer) session.getAttribute("benutzer");
        if (user == null || !user.isAngemeldet()) {
            return "redirect/login";
        }

        String t;
        String error1 = "";
        String error2 = "";
        String error3 = "";
        String error4 = "";

        // ausgebedatum eingegeben?
        if ((t = anfrage.getAusgabeTermin().getDatumString()) == null || t.length() != 10) {
            error1 = F25;
            model.addAttribute("date1", true);
        } else {
            anfrage.getAusgabeTermin().setDatum(Date.valueOf(t));
        }

        // rückgabedatum eingegeben?
        if ((t = anfrage.getRueckgabeTermin().getDatumString()) == null || t.length() != 10) {
            error1 = F25;
            model.addAttribute("date2", true);
        } else {
            anfrage.getRueckgabeTermin().setDatum(Date.valueOf(t));
        }

        // ausgabeUhrzeit eingegeben?
        if ((t = anfrage.getAusgabeTermin().getUhrzeitString()) == null || t.length() != 5) {
            error2 = F26;
            model.addAttribute("time1", true);

        } else {
            anfrage.getAusgabeTermin().setUhrzeit(Time.valueOf(t + ":00"));
        }

        // rückgabeuhrzeit eingegeben?
        if ((t = anfrage.getRueckgabeTermin().getUhrzeitString()) == null || t.length() != 5) {
            error2 = F26;
            model.addAttribute("time2", true);
        } else {
            anfrage.getRueckgabeTermin().setUhrzeit(Time.valueOf(t + ":00"));
        }

        // uebergabeort gewählt?
        if (anfrage.getAusgabeTermin().getOrt() == null) {
            error3 += F27;
            model.addAttribute("ort1", true);
        }

        // rückgabeort gewählt?
        if (anfrage.getRueckgabeTermin().getOrt() == null) {
            error4 += F27;
            model.addAttribute("ort2", true);
        }

        // Beide Datumeingaben getätigt
        if (error1.length() == 0) {
            // Ausleihdatum vor rückgabedatum?
            if (anfrage.getAusgabeTermin().getDatum().after(anfrage.getRueckgabeTermin().getDatum()) || anfrage
                    .getAusgabeTermin().getDatumString().equals(anfrage.getRueckgabeTermin().getDatumString())) {
                model.addAttribute("date1", true);
                model.addAttribute("date2", true);
                error1 += F23;
            }
            // ausleihdatum in der vergangenheit?
            if (anfrage.getAusgabeTermin().getDatum().before(new Date(System.currentTimeMillis()))) {
                model.addAttribute("date1", true);
                if (error1.length() != 0)
                    error1 += "<br/>";
                error1 += F24;
            }
        }

        Artikel artikel = artikelDAO.findOne(id);

        if (error1.length() != 0 || error2.length() != 0 || error3.length() != 0 || error4.length() != 0) {
            model.addAttribute("error1", error1);
            model.addAttribute("error2", error2);
            model.addAttribute("error3", error3);
            model.addAttribute("error4", error4);
            model.addAttribute("artikel", artikel);
            return "ausleihanfrage";
        }

        anfrage.setArtikel(artikel);
        anfrage.setStatus(Status.offen);
        anfrage.setAusleiher(user);
        anfrage.setVerleiher(artikel.getVerleiher());
        user.getGesendeteAnfragen().add(anfrage);
        anfrageDAO.save(anfrage);
        System.out.println(anfrage.getAusgabeTermin().getUhrzeitString() + " "
                + anfrage.getAusgabeTermin().getDatumString() + "    " + anfrage.getRueckgabeTermin().getUhrzeitString()
                + "  " + anfrage.getRueckgabeTermin().getDatumString());

        return ("redirect:/dashboard");
    }

    /**
     * Der Zustand von Anfrage erstellen wird aufgerufen
     * @param id Anfrage Id
     * @param model Artikel wird hinzugefügt
     * @param session für den angemeldeten Benutzer. Darf nicht null sein.
     * @param anfrage
     * @return Benutzer ist nicht angemeldet. Man wird auf die Loginseite geleitet
     * @return ausleihanfrage wurde aufgerufen und aktualisiert
     */
    @GetMapping("/ausleihanfrage/{artikelId}")
    public String anfrageErstellen(@PathVariable("artikelId") long id, Model model, HttpSession session,
                                   @ModelAttribute Anfrage anfrage) {
        Benutzer user = (Benutzer) session.getAttribute("benutzer");
        if (user == null || !user.isAngemeldet()) {
            return "redirect:/login";
        }
        Artikel artikel = artikelDAO.findOne(id);
        if (artikel == null)
            return "redirect:/dashboard";
        model.addAttribute("artikel", artikel);
        return ("ausleihanfrage");
    }


}
