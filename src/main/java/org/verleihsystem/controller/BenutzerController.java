package org.verleihsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.verleihsystem.dao.BenutzerDAO;
import org.verleihsystem.model.Benutzer;
import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.sql.Time;

/**
 * @author Hakim, tarik56
 * Controller für das Registrieren von Nutzern
 */
@Controller
public class BenutzerController {

    /**
     * BenutzerDAO
     */
    @Autowired
    private BenutzerDAO benutzerDAO;

    /**
     * GetMapping
     * @return Weiterleitung zur Registrierung Formular
     */
    @GetMapping("/registrierung")
    public String getRegistrieren() {
        return "registrierung";
    }

    /**
     * Serverseitige Abfrage und Bearbeitung der Registrierung
     *
     * @param rq HttpServletRequest
     * @param model Model
     * @return Weiterleitung zum Login im Falle der erfolgreichen Registrierung,
     *         ansonsten weiterleitung zum Registrierungsformular mit entsprechenden
     *         Warnhinweisen bezüglich der Patten oa.
     */
    @PostMapping("/registrierung")
    public String postRegistrieren(HttpServletRequest rq, Model model) {
        boolean namePattern = true;
        boolean emailPattern = true;
        boolean passwortPattern = true;
        if (rq.getParameter("name") != null) {
            if (!rq.getParameter("name").matches("^[A-Za-z0-9_]{8,20}$")) {
                model.addAttribute("failure1", true);
                namePattern = false;
            }
            if (!rq.getParameter("email").matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$")) {
                model.addAttribute("failure2", true);
                emailPattern = false;
            }
            if (!rq.getParameter("password").matches("(?=.{12,}$)(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*\\W).*")) {
                model.addAttribute("failure3", true);
                passwortPattern = false;
            }
            if (!namePattern || !emailPattern || !passwortPattern) {
                return "registrierung";
            }
            if (!rq.getParameter("passwordWdhg").equals(rq.getParameter("password"))) {
                model.addAttribute("failure", true);
                return "registrierung";
            } else {
                Benutzer benutzer = new Benutzer();
                benutzer.isAngemeldet();
                benutzer.setBenutzername(rq.getParameter("name"));
                benutzer.setPasswort(rq.getParameter("password"));
                benutzer.setEmailAdresse(rq.getParameter("email"));
                benutzer.setLetzteAktivitätDatum(new Date(5));
                benutzer.setLetzteAktivitätZeit(new Time(5));
                benutzerDAO.save(benutzer);
                return "redirect:login";
            }
        }
        return "redirect:registrierung";
    }

}
