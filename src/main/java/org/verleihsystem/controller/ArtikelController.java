package org.verleihsystem.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.verleihsystem.dao.ArtikelDAO;
import org.verleihsystem.dao.ArtikelkategorieDAO;
import org.verleihsystem.dao.PfandDAO;
import org.verleihsystem.dao.VerleihgebuehrDAO;
import org.verleihsystem.model.Artikel;
import org.verleihsystem.model.Artikelkategorie;
import org.verleihsystem.model.Benutzer;
import org.verleihsystem.model.GebuehrArt;
import org.verleihsystem.model.Ort;
import org.verleihsystem.model.Pfand;
import org.verleihsystem.model.PfandArt;
import org.verleihsystem.model.Verleihgebuehr;
import org.verleihsystem.model.validation.ArtikelFormValidator;
import org.verleihsystem.viewmodel.ArtikelForm;


/**
 * @author dschiko,embeach
 * Controller für saemtliche Endpunkte im Zusammenhang mit Artikeln
 */
@Controller
public class ArtikelController extends WebMvcConfigurerAdapter {
	
	/**
	 * Data access object (DAO) für Artikel
	 * Schnittstelle zur Datenbank für die Artikel
	 * @author dschiko
	 */
	@Autowired
	private ArtikelDAO artikelDAO;
	/**
	 * Data access object (DAO) für Artikelkategorien
	 * Schnittstelle zur Datenbank für die Artikelkategorien
	 * @author dschiko
	 */
	@Autowired
	private ArtikelkategorieDAO artikelkategorieDAO;
	/**
	 * Data access object (DAO) für Verleihgebuehren
	 * Schnittstelle zur Datenbank für die Verleihgebuehren
	 * @author dschiko
	 */
	@Autowired
	private VerleihgebuehrDAO verleihgebuehrDAO;
	/**
	 * Data access object (DAO) für Pfand
	 * Schnittstelle zur Datenbank für Pfand
	 * @author dschiko
	 */
	@Autowired
	private PfandDAO pfandDAO;
	
	
	@GetMapping("/artikel/{artikelId}")
	public String artikelDetailansicht(@PathVariable("artikelId") long id, HttpSession session, Model model) {
		Artikel artikel = artikelDAO.findOne(id);
		Benutzer user = (Benutzer) session.getAttribute("benutzer");
		if (user != null)
			model.addAttribute("login", user.isAngemeldet());
		if (artikel.getVerleiher().getBenutzername().equals(user.getBenutzername())) {
			model.addAttribute("login", false);
		}
		model.addAttribute("artikel", artikel);
		return "artikel";
	}
	

	/**
	 * Der inital Zustand des Artikel-Eintragen-Formulars wird erzeugt
	 * @author dschiko,embeach
	 * @param m Kontext für das Artikel-Eintragen-Formular. Darf nicht null sein.
	 * @param s Session für den angemeldeten Benutzer. Darf nicht null sein.
	 * @return Die initale View für das Artikel-Eintragen-Formular
	 */
	@RequestMapping(value = "artikelEintragen", method = RequestMethod.GET)
	public String neuerArtikel(final Model m, final HttpSession s) {
		if(!isAngemeldet(s)) return "redirect:/login";
		final ArtikelForm af = new ArtikelForm();
		final Artikel a = Artikel.createEmptyArtikel();
		af.setArtikel(a);
		af.getArtikel().getUebergabeorte().add(new Ort());
		m.addAttribute("artikelForm", af);
		return "artikelEintragen";
	}


	/**
	 * Persistieren eines neuen Artikels und anschließende Weiterleitung zur Artikel-Detailansicht
	 * Formulareingaben werden Serverseitig validiert über die Validierungsgruppe FormValidation
	 * @author dschiko,embeach
	 * @param af Viewmodel für das Artikel-Eintragen-Formular. Darf nicht null sein.
	 * @param result Ergebnis der Validierung. Darf nicht null sein.
	 * @param s Session für den angemeldeten Benutzer. Darf nicht null sein.
	 * @return Weiterleitung zur Artikel-Detailansicht
	 */
	@RequestMapping(value = "artikelEintragen", method = RequestMethod.POST)
	public String artikelEintragen(@Validated(ArtikelForm.FormValidation.class) @ModelAttribute(value = "artikelForm") final ArtikelForm af, final BindingResult result, final HttpSession s) {
		if(!isAngemeldet(s)) return "redirect:/login";
		final ArtikelFormValidator artikelFormValidator = new ArtikelFormValidator();
		artikelFormValidator.validate(af, result);
		if(result.hasErrors()) return "artikelEintragen";
		Benutzer v=(Benutzer)s.getAttribute("benutzer");
		final Artikel a = af.getArtikel();
		a.setVerleiher(v);
		a.getUebergabeorte().forEach(o -> o.setArtikel(a));
		final Pfand p = a.getPfand();
		p.setPfandArt(PfandArt.disabledPfandArt); 
		final Verleihgebuehr vg = a.getGebuehr();
		vg.setGebuehrArt(GebuehrArt.createFromString(af.getFrequenz()));
		p.setArtikel(a);
		vg.setArtikel(a);
		final Artikel a_ = this.artikelDAO.save(a);
		this.pfandDAO.save(p);
		this.verleihgebuehrDAO.save(vg);
		return "redirect:/artikel/" + a_.getId()+"?new="+true;
	}
	
	/**
	 * Pruefen ob Benutzer angemeldet ist
	 * @author dschiko,embeach
	 * @param s Session für den angemeldeten Benutzer. Darf nicht null sein.
	 * @return Benutzer angemeldet ?
	 */
	private boolean isAngemeldet(final HttpSession s) {
		final Benutzer b = (Benutzer) s.getAttribute("benutzer");
		return b != null && b.isAngemeldet();
	}

	
	/**
	 * Hinzufuegen eines weiteren Uebergabeorts im Artikel-Eintragen-Formular 
	 * @author dschiko
	 * @param af Viewmodel für das Artikel-Eintragen-Formular. Darf nicht null sein.
	 * @param o Neuer Uebergabeort. Darf nicht null sein.
	 * @param s Session für den angemeldeten Benutzer. Darf nicht null sein.
	 * @return Artikel-Eintragen-Formular mit neuen Uebergabeort
	 */
	@RequestMapping(value = "artikelEintragen", params="uebergabeOrtHinzufuegen" ,method = RequestMethod.POST)
	public String uebergabeOrtHinzufuegen(final @ModelAttribute(value = "artikelForm") ArtikelForm af, final Ort o, final HttpSession s) {
		if(!isAngemeldet(s)) return "redirect:/login";
		final Artikel a = af.getArtikel();
		final int index = a.getUebergabeorte().size();
		a.getUebergabeorte().add(index,o);
		return "artikelEintragen";
	}
	

	/**
	 * Loeschen eines Uebergabeorts im Artikel-Eintragen-Formular 
	 * @author dschiko
	 * @param af Viewmodel für das Artikel-Eintragen-Formular. Darf nicht null sein.
	 * @param s Session für den angemeldeten Benutzer. Darf nicht null sein.
	 * @param index Position des zu loeschenen Uebergabeorts. Darf nicht null sein.
	 * @return Artikel-Eintragen-Formular mit geloeschten Uebergabeort
	 */
	@RequestMapping(value = "artikelEintragen", params="uebergabeOrtEntfernen" ,method = RequestMethod.POST)
	public String uebergabeOrtEntfernen(final @ModelAttribute(value = "artikelForm") ArtikelForm af, final HttpSession s, final @RequestParam("uebergabeOrtEntfernen") String index) {
		if(!isAngemeldet(s)) return "redirect:/login";
		final Artikel a = af.getArtikel();
		final int i = Integer.valueOf(index); 
		a.getUebergabeorte().remove(i);
		return "artikelEintragen";
	}
	
	/**
	 * Menge der Moeglichkeiten eine Gebuehr zu zahlen
	 * @author dschiko,embeach
	 * @return Frequenzen für die Gebuehrart z.B. taeglich
	 */
	@ModelAttribute("frequenzen")
    public GebuehrArt[] getFrequenzen() {
        return GebuehrArt.getFrequenzen();
	}
	
	
	/**
	 * Menge der Artikelkategorien
	 * @author dschiko,embeach
	 * @return Alle Artikelkategorien
	 */
	@ModelAttribute("kategorien")
    public Iterable<Artikelkategorie> getKategorien() {
        return this.artikelkategorieDAO.findAll();
	}
}
