package org.verleihsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.verleihsystem.dao.AnfrageDAO;
import org.verleihsystem.dao.ArtikelDAO;
import org.verleihsystem.dao.BenutzerDAO;
import org.verleihsystem.model.Benutzer;

import javax.servlet.http.HttpSession;


@Controller
public class MainController extends WebMvcConfigurerAdapter {

    private static final String F23 = "Anfangsdatum muss vor Rueckgabedatum liegen";
    private static final String F24 = "Zeitraum befindet sich vor aktuellem Datum";
    private static final String F25 = "Uebergabe-Datum muss angegeben werden";
    private static final String F26 = "Uebergabe-Uhrzeit muss angegeben werden";
    private static final String F27 = "Uebergabeort muss aus der Liste gewaehlt werden";

    @Autowired
    private BenutzerDAO benutzerDAO;
    @Autowired
    private ArtikelDAO artikelDAO;
    @Autowired
    private AnfrageDAO anfrageDAO;

    @RequestMapping("/")
    public String home(HttpSession session) {
        Benutzer user = (Benutzer) session.getAttribute("benutzer");
        if (user == null || !user.isAngemeldet())
            return "redirect:/login";
        return "redirect:/dashboard";
    }


    @GetMapping("/login")
    public String showLogin(@ModelAttribute Benutzer benutzer, HttpSession session) {
        if (session.getAttribute("benuzter") != null && ((Benutzer) session.getAttribute("benutzer")).isAngemeldet()) {
            return "redirect:/dashboard";
        }
        return "login";
    }

    @PostMapping("/login")
    public String postLogin(@ModelAttribute Benutzer benutzer, Model model, HttpSession session) {
        Benutzer foundUser = benutzerDAO.getBenutzerByBenutzername(benutzer.getBenutzername());

        if (foundUser == null || !foundUser.getPasswort().equals(benutzer.getPasswort())) {
            model.addAttribute("failure", true);
            return "login";
        } else {
            foundUser.setAngemeldet(true);
            session.setAttribute("benutzer", foundUser);
            return "redirect:/dashboard";
        }
    }

    @GetMapping("/logout")
    public String getLogout(HttpSession session) {
        Benutzer user = (Benutzer) session.getAttribute("benutzer");
        if (user != null) {
            session.removeAttribute("benutzer");
            user.setAngemeldet(false);
        }
        return "redirect:/login";
    }

    @GetMapping("/dashboard")
    public String dashboard(Model model, HttpSession session) {
        Benutzer user = (Benutzer) session.getAttribute("benutzer");
        if (user == null || !user.isAngemeldet()) {
            return "redirect:login";
        }
        Benutzer dbUser = benutzerDAO.getBenutzerByBenutzername(user.getBenutzername());
        model.addAttribute("artikelliste", dbUser.getArtikel());
        model.addAttribute("gesendeteAnfragen", dbUser.getGesendeteAnfragen());
        model.addAttribute("empfangeneAnfragen", dbUser.getEmpfangeneAnfragen());
        return "dashboard";
    }


    @RequestMapping("/suche")
    public String suche(Model model) {
        model.addAttribute("artikelliste", artikelDAO.findAll());
        return "suche";
    }
}

