package org.verleihsystem.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.verleihsystem.model.Artikel.DBValidation;
import org.verleihsystem.viewmodel.ArtikelForm;

/**
 * Gibt die Pfandkonditionen des Artikels an
 * Es kann ein Pfandbetrag festgelegt werden.
 * Es können alternativ akzeptierte PfandGegenstände angegeben werden (Freitext) zB Führerschein oder Schlüssel
 * Das Feld pfandArt ist deprecated und sollte langfristig entfernt werden weil es redundant ist.
 * @author embeach
 *
 */
@Entity
@Table(name = "pfand")
public class Pfand {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	/**
	 * Es kann ein Pfandbetrag in Eur festgelegt werden.
	 * TODO der Datentyp muss ggf auf 2 Stellen hinterm Komma eingeschränkt werden
	 */
	private double pfandBetrag;

	/**
	 * Es können alternativ akzeptierte PfandGegenstände angegeben werden (Freitext) zB Führerschein oder Schlüssel
	 */
	@NotNull(groups= {ArtikelForm.FormValidation.class,DBValidation.class})
	private String pfandGegenstand="";
	/**
	 * Das Feld pfandArt ist deprecated und sollte langfristig entfernt werden weil es redundant ist.
	 * @deprecated
	 */
	@NotNull(groups= {DBValidation.class})
	@Enumerated(EnumType.ORDINAL)
	private PfandArt pfandArt;//achtung! neuanordnung oder erweiterung der enum macht alte daten unbrauchbar - falls das in betracht kommt, sieh zB https://dzone.com/articles/mapping-enums-done-right 
	/**
	 * zugehöriger Artikel
	 */
	@NotNull(groups= {DBValidation.class})
	@OneToOne
	@JoinColumn(name="artikel_id")
	private Artikel artikel;//abweichend von Klassendiagramm hinzugefügt, da unidirektional komplizierter wäre
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public double getPfandBetrag() {
		return pfandBetrag;
	}
	public void setPfandBetrag(double pfandBetrag) {
		this.pfandBetrag = pfandBetrag;
	}
	public String getPfandGegenstand() {
		return pfandGegenstand;
	}
	public void setPfandGegenstand(String pfandGegenstand) {
		this.pfandGegenstand = pfandGegenstand;
	}
	public PfandArt getPfandArt() {
		return pfandArt;
	}
	public void setPfandArt(PfandArt pfandArt) {
		this.pfandArt = pfandArt;
	}
	public Artikel getArtikel() {
		return artikel;
	}
	public void setArtikel(Artikel artikel) {
		this.artikel = artikel;
	}

}
