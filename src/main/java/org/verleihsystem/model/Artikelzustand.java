package org.verleihsystem.model;
//achtung! neuanordnung oder erweiterung der enum macht alte daten unbrauchbar - falls das in betracht kommt, sieh zB https://dzone.com/articles/mapping-enums-done-right 
/**
 * Artikelzustand
 * es sollte evt nochmal evaluiert werden ob sich ein enum eignet
 * @author embeach,dschiko
 *
 */
public enum Artikelzustand {

	wieNeu("Artikel wie neu"),

	wenigGebraucht("Artikel wurde wenig gebraucht"),

	normaleGebrauchsspuren("Artikel hat normale Gebrauchtsspuren"),

	starkeGebrauchsspuren("Artikel hat starke Gebrauchtsspuren"),

	leichteBeschaedigungen("Artikel hat leichte Beschädigungen"),


	starkeBeschaedigungen("Artikel hat starke Beschädigungen");
	
	private final String beschreibung;
	
	Artikelzustand(String b){
		this.beschreibung = b;
	}
	
	public String getBeschreibung(){
		return this.beschreibung;
	}
	
}
