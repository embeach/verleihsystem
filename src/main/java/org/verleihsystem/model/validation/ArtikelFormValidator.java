package org.verleihsystem.model.validation;


import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.verleihsystem.model.Artikel;
import org.verleihsystem.model.GebuehrArt;
import org.verleihsystem.model.Ort;
import org.verleihsystem.viewmodel.ArtikelForm;

/**
 * Das Vorgehen fürs Formular validieren ist: Es wird zunächst in Artikel validiert, 
 * wenn das nicht möglich ist wird in ArtikelForm validiert, 
 * wenn das nicht möglich ist wird in ArtikelValidator valdiert.
 * @author embeach
 *
 */
public class ArtikelFormValidator implements Validator{

	/**
	 * @author embeach
	 */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class artikel) {
      return Artikel.class.equals(artikel);
    }

    /**
     * prüft 1. das frequenz feld auf fehlerhafte Beschreibung (nur möglich falls an der Website vorbei ein Post gesendet wird)
     * und prüft 2. die Übergabeorte Liste ob sie mindestens einen Eintrag enthält und nicht in allen feldern Leer ist.
     * Precondition zu 1.: Für die frequenz (Bezugsintervall für zeitliche Verleihgebühr) werden @NotBlank messages bereits vorher abgehandelt.
     * Precondition zu 2.: Unvollständige einzelne Felder im Übergabeort werden bereits abgehandelt.
     * @author embeach
     */
    @Override
    public void validate(Object target, Errors errors) {
      ArtikelForm artikelForm = (ArtikelForm) target;
      Artikel artikel=artikelForm.getArtikel();
      //assumes that for @NotBlank a message is already generated
      if(artikelForm.getFrequenz()!=null&&GebuehrArt.createFromString(artikelForm.getFrequenz())==null) {
    	  errors.rejectValue("frequenz","your_error_code", null, "kein Intervall für zeitliche Gebühr gewählt (fehlerhaft)");
      }
      //assumes invalid specific fields from uebergabeort are already handled
      List<Ort> ueOrte=artikel.getUebergabeorte();
      if(ueOrte==null||ueOrte.size()==0||ueOrte.get(0).isEmpty()) {
    	  errors.rejectValue("artikel.uebergabeorte[0]","your_error_code", null, "Übergabeort​ ​ muss​ ​ angegeben​ ​ werden (F17)");
      }
    }

}