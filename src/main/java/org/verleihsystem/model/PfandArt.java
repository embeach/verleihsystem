package org.verleihsystem.model;
//achtung! neuanordnung oder erweiterung der enum macht alte daten unbrauchbar - falls das in betracht kommt, sieh zB https://dzone.com/articles/mapping-enums-done-right 

/**
 * hat ehemals die PfandArt angegeben. Es ist aber redundant (siehe Pfand).
 * es soll nun disabledPfandArt genutzt werden bis die Datenbank umgestellt wird.
 * deprecated
 *
 * @author embeach
 */
public enum PfandArt {
    keinPfand("kein Pfand"),
    pfandGegenstand("Pfandgegenstand"),
    pfandBetrag("Pfandbetrag"),
    disabledPfandArt("disabledPfandArt");


    private final String beschreibung;

    private PfandArt(String b) {
        this.beschreibung = b;
    }


}

