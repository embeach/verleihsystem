package org.verleihsystem.model;
//achtung! neuanordnung oder erweiterung der enum macht alte daten unbrauchbar - falls das in betracht kommt, sieh zB https://dzone.com/articles/mapping-enums-done-right 

/**
 * Die Klasse gibt das Bezugsintervall für die zeitbezogene Verleihgebühr an.
 * Die Felder keineGebuehr und festerBetrag sind deprecated und sollen langfristig entfernt werden.
 * Der Klassenname soll langfristig umbenannt werden zB zu Frequenz evt findet sich ein besserer Name.
 * @author embeach
 *
 */
public enum GebuehrArt {//deprecated! should be renamed to "Frequenz"
	/**
	 *deprecated
	 */
	keineGebuehr("keine Gebühr"),//deprecated! should be removed
	/**
	 * deprecated
	 */
	festerBetrag("fester Betrag"),//deprecated! should be removed
	taeglich("täglich"),//default
	stuendlich("stündlich"),
	woechentlich("wöchentlich"),
	monatlich("monatlich");
	
	private final String beschreibung;
	
	GebuehrArt(String b){
		this.beschreibung = b;
	}

	public String toString(){
		return this.beschreibung;
	}

	
	/**
	 * Liefert die zur Beschreibung passende GebuehrArt.
	 * Die Felder keineGebuehr und festerBetrag sind deprecated und werden daher nicht zurückgegeben.
	 * siehe auch klassenbeschreibung
	 * @author embeach
	 * @param beschr beschr!=null
	 * @return GebuehrArt aus (taeglich,stuendlich,woechentlich,monatlich) oder null
	 */
	public static GebuehrArt createFromString(String beschr) {
		assert(beschr!=null);
		if(beschr.equals(GebuehrArt.taeglich.toString())) {
			return GebuehrArt.taeglich;
		}else if(beschr.equals(GebuehrArt.stuendlich.toString())) {
			return GebuehrArt.stuendlich;
		}else if(beschr.equals(GebuehrArt.woechentlich.toString())) {
			return GebuehrArt.woechentlich;
		}else if(beschr.equals(GebuehrArt.monatlich.toString())) {
			return GebuehrArt.monatlich;
		}else {
			return null;
		}
		
	}

	/**
	 * gibt die Liste der "Frequenzen" bzw Bezugsintervalle für die zeitliche Verleihgebühr zurück.
	 * @author embeach
	 * @return GebuehrArt[4], {GebuehrArt.stuendlich, GebuehrArt.taeglich, GebuehrArt.woechentlich, GebuehrArt.monatlich}
	 */
	public static GebuehrArt[] getFrequenzen() {
        return new GebuehrArt[] {GebuehrArt.stuendlich, GebuehrArt.taeglich, GebuehrArt.woechentlich, GebuehrArt.monatlich};
	}
}
