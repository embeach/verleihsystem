package org.verleihsystem.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.verleihsystem.viewmodel.ArtikelForm;

/**
 * Model für Artikel.
 * Es enhält sowohl Datenbank-Validierungen als auch Formular-Validierungen.
 * Das Vorgehen fürs Formular validieren ist: Es wird zunächst in Artikel validiert, 
 * wenn das nicht möglich ist wird in ArtikelForm validiert, 
 * wenn das nicht möglich ist wird in ArtikelValidator valdiert.
 * @author embeach
 *
 */
@Entity
@Table(name = "artikel")
public class Artikel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	/**
	 * Überschrift des Artikelangebots
	 */
	@NotBlank(message="Artikelname​ ​ muss​ ​ angegeben​ ​ werden (F12)",groups= {ArtikelForm.FormValidation.class,DBValidation.class})
	private String name="";
	/**
	 * Beschreibung des Artikels
	 */
	@NotBlank(message="Artikelbeschreibung​ ​ muss​ ​ angegeben​ ​ werden (F13)",groups= {ArtikelForm.FormValidation.class,DBValidation.class})
	private String beschreibung="";
	/**
	 * Technische Daten des Artikels
	 */
	@NotNull(groups= {ArtikelForm.FormValidation.class,DBValidation.class})
	private String technischeDaten="";
	/**
	 * Liste der Übergabeorte wo die Übergabe des Artikels möglich ist.
	 */

	@NotEmpty(groups= {ArtikelForm.FormValidation.class,DBValidation.class})
	@OneToMany(cascade= CascadeType.ALL,mappedBy="artikel",orphanRemoval=true)
	@Valid
	private List<Ort> uebergabeorte=new ArrayList<>();

	/**
	 * Pfand Konditionen des Artikels
	 */
	@NotNull(groups= {ArtikelForm.FormValidation.class,DBValidation.class})
	@Valid
	@OneToOne(mappedBy = "artikel", cascade=CascadeType.ALL,orphanRemoval=true)
	private Pfand pfand;

	/**
	 * Verleihgebühr Konditionen des Artikels
	 */
	@NotNull(groups= {ArtikelForm.FormValidation.class,DBValidation.class})
	@Valid
	@OneToOne(mappedBy="artikel", cascade=CascadeType.ALL,orphanRemoval=true)
	private Verleihgebuehr gebuehr;

	/**
	 * Artikelkategorie des Artikels - zB Werkzeug. Die Angabe ist optional.
	 */
	//@NotNull(groups= {ArtikelForm.FormValidation.class,DBValidation.class})//optional..
	@ManyToOne
	@JoinColumn(name="kategorie_id")
	private Artikelkategorie kategorie;
	/**
	 * Artikelzustand zB wenig gebraucht
	 */
	@NotNull(groups= {ArtikelForm.FormValidation.class,DBValidation.class},message="Artikelzustand​ ​ muss​ ​ angegeben​ ​ werden (F16)")
	@Enumerated(EnumType.ORDINAL)
	private Artikelzustand zustand;//achtung! neuanordnung oder erweiterung der enum macht alte daten unbrauchbar - falls das in betracht kommt, sieh zB https://dzone.com/articles/mapping-enums-done-right 
	/**
	 * Bilder vom Artikel
	 */
	@OneToMany(cascade= CascadeType.ALL,mappedBy="artikel",orphanRemoval=true)
	private List<Bild> bilder=new ArrayList<>();
	/**
	 * Anfragen die den Artikel betreffen
	 */
	@OneToMany(mappedBy="artikel",cascade= CascadeType.ALL,orphanRemoval=true)//falls Verleiher Artikel löschen können soll, muss "gelöscht"-flag eingefügt werden, endgültige löschung erst nachdem anfragen genügend in vergangeheit liegen (fristen)
	private List<Anfrage> anfragen;//abweichend von Klassendiagramm hinzugefügt (für Berechnung der Verfügbarkeit hilfreich)

	/**
	 * der Verleiher des Artikels
	 */
	@NotNull(groups= {DBValidation.class})
	@ManyToOne
	@JoinColumn(name="verleiher_id")
	private Benutzer verleiher;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBeschreibung() {
		return beschreibung;
	}
	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}
	public String getTechnischeDaten() {
		return technischeDaten;
	}
	public void setTechnischeDaten(String technischeDaten) {
		this.technischeDaten = technischeDaten;
	}
	public List<Ort> getUebergabeorte() {
		return uebergabeorte;
	}
	public void setUebergabeorte(List<Ort> uebergabeorte) {
		this.uebergabeorte = uebergabeorte;
	}
	public Pfand getPfand() {
		return pfand;
	}
	public void setPfand(Pfand pfand) {
		this.pfand = pfand;
	}
	public Verleihgebuehr getGebuehr() {
		return gebuehr;
	}
	public void setGebuehr(Verleihgebuehr gebuehr) {
		this.gebuehr = gebuehr;
	}
	public Artikelkategorie getKategorie() {
		return kategorie;
	}
	public void setKategorie(Artikelkategorie kategorie) {
		this.kategorie = kategorie;
	}
	public Artikelzustand getZustand() {
		return zustand;
	}
	public void setZustand(Artikelzustand zustand) {
		this.zustand = zustand;
	}
	public List<Bild> getBilder() {
		return bilder;
	}
	public void setBilder(List<Bild> bilder) {
		this.bilder = bilder;
	}
	public List<Anfrage> getAnfragen() {
		return anfragen;
	}
	public void setAnfragen(List<Anfrage> anfragen) {
		this.anfragen = anfragen;
	}
	public Benutzer getVerleiher() {
		return verleiher;
	}
	public void setVerleiher(Benutzer verleiher) {
		this.verleiher = verleiher;
	}
	/**
	 * Technische Darstelltung aller Felder des Artikels
	 * @author embeach
	 */
	@Override
	public String toString() {
		return "Artikel [id=" + id + ", name=" + name + ", beschreibung=" + beschreibung + ", technischeDaten="
				+ technischeDaten + ", uebergabeorte=" + uebergabeorte + ", pfand=" + pfand + ", gebuehr=" + gebuehr
				+ ", kategorie=" + kategorie + ", zustand=" + zustand + ", bilder=" + bilder + ", anfragen=" + anfragen
				+ ", verleiher=" + verleiher + "]";
	}
	/**
	 * Hilfsmethode für das ArtikelEintragen Formular
	 * @author embeach
	 * @return ein "leerer" Artikel
	 */
	public static Artikel createEmptyArtikel() {
		Artikel a=new Artikel();
		a.setGebuehr(new Verleihgebuehr());
		a.setPfand(new Pfand());
		a.setKategorie(new Artikelkategorie());
		return a;
		
	}
	/**
	 * Gruppe für die DB-Validierung
	 * TODO die DB validierung wird bislang nicht abgefragt
	 * @author embeach
	 *
	 */
	interface DBValidation{}
	
}
