package org.verleihsystem.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.verleihsystem.model.Artikel.DBValidation;
import org.verleihsystem.viewmodel.ArtikelForm;
/**
 * Ort an dem ein Artikel übergeben werden kann oder werden soll.
 * Die praktikabilität des Adressformat sollte ggf nochmal evaluiert werden
 * @author embeach
 *
 */

@Entity
@Table(name = "orte")
public class Ort {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/**
	 * Stadt zB "Lübeck"
	 */
	@NotBlank(groups= {ArtikelForm.FormValidation.class,DBValidation.class},message="Pflichtfeld")
	private String stadt="";
	/**
	 * Postleitzahl zB "23562"
	 */
	@NotBlank(groups= {ArtikelForm.FormValidation.class,DBValidation.class},message="Pflichtfeld")
	private String plz="";
	/**
	 * Straße zB "Lange Str."
	 */
	@NotBlank(groups= {ArtikelForm.FormValidation.class,DBValidation.class},message="Pflichtfeld")
	private String strasse="";
	/**
	 * Hausnummer zB "2a"
	 */
	@NotBlank(groups= {ArtikelForm.FormValidation.class,DBValidation.class},message="Pflichtfeld")
	private String nummer="";
	/**
	 * Beschreibung zB "links vom Haus auf der Wiese"
	 */
	@NotNull(groups= {ArtikelForm.FormValidation.class,DBValidation.class})
	private String beschreibung="";
	@NotNull(groups= {DBValidation.class})
	@ManyToOne
	@JoinColumn(name="artikel_id")
	private Artikel artikel;//abweichend von Klassendiagramm hinzugefügt, da unidirektional komplizierter wäre

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStadt() {
		return stadt;
	}

	public void setStadt(String stadt) {
		this.stadt = stadt;
	}

	public String getPlz() {
		return plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getNummer() {
		return nummer;
	}

	public void setNummer(String nummer) {
		this.nummer = nummer;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public Artikel getArtikel() {
		return artikel;
	}

	public void setArtikel(Artikel artikel) {
		this.artikel = artikel;
	}
	
	public boolean isEmpty() {
		return stadt.isEmpty()&&plz.isEmpty()&&strasse.isEmpty()&&nummer.isEmpty()&&beschreibung.isEmpty();
	}



}
