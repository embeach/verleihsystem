[![pipeline status](https://gitlab.com/embeach/verleihsystem/badges/master/pipeline.svg)](https://gitlab.com/embeach/verleihsystem/commits/master)
[![coverage report](https://gitlab.com/embeach/verleihsystem/badges/master/coverage.svg)](https://gitlab.com/embeach/verleihsystem/commits/master)

# Verleihsystem  

## English:

System for lending/borrowing things. For individuals or associations.

This is an experimental training project with very limited features and should not be used in production without major adjustments.

The project was developt as training project at TH-Lübeck in 2018. The corresponding developer-team is noted in the LICENSE-File. The code is licensed unter MIT-License. The commit-history and issues of the original repository havn't been published to avoid possible privacy-issues.

## Deutsch: 

System um Dinge zum Verleih anzubieten und auszuleihen. Für Privatpersonen und Vereine.

Dies ist ein experimentelles Übungsprojekt mit sehr begrenztem Funktionsumfang und sollte ohne erhebliche Anpassungen nicht in Produktion eingesetzt werden.

Das Projekt wurde als Übungsprojekt im Rahmen der TH-Lübeck 2018 entwickelt. Das zugehörige Entwicklerteam ist im LICENSE-File aufgeführt. Der Code steht unter der MIT-Lizenz. Die Commit-History und Issues des ursprünglichen Repository wurden nicht veröffentlicht, um mögliche Privacy-Issues zu vermeiden. 
